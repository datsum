# vim: set encoding=utf-8 noet ts=4
# TODO: Better recursive multi-track (bin+cue) support
# TODO: ClrMamePro, ListXML, RomCenter

"""Parse DAT files for ROM management."""

import xml.etree.ElementTree as ET
from pathlib import Path

class DAT:
	"""
	DAT (ROM Management Datafile).
	"""
	header = {}
	name = ""
	sums = []

	def csv(self):
		"""
		Print CRC-32 to csv4 (Comma Seperated Value) format.
		"""

		for i in self.sums:
			try:
				path, name = i["name"].split("/")
			except ValueError:
				name = i["name"]
				path = ""

			print("\"{0}\",{1},{2},\"{3}\",".format(
				name, i["size"], i["crc"], path
			))

	def name(self, crc, size):
		"""
		Get rom name from crc+size.
		"""

		for i in self.sums:
			if i["crc"] == crc and i["size"] == size:
				return i["name"]

	def md5sum(self):
		"""
		Print MD5 to md5sum (GNU coreutils) format.
		"""

		for i in self.sums:
			print("{0}  {1}/{2}".format(i["md5"], self.name, i["name"]))

	def sfv(self):
		"""
		Print CRC-32 to SFV (Simple File Verification) format.
		"""

		print(";\n; DAT file converted by datsum")
		print(";\n; Name:    {0}".format(self.header["name"]))
		print("; Version: {0}".format(self.header["version"]))
		print("; Website: {0}\n;".format(self.header["url"]))

		for i in self.sums:
			print("{0}/{1} {2}".format(self.name, i["name"], i["crc"]))

	def sha1sum(self):
		"""
		Print SHA1 to sha1sum (GNU coreutils) format.
		"""

		for i in self.sums:
			print("{0}  {1}/{2}".format(i["sha1"], self.name, i["name"]))

class ClrMamePro:
	"""
	ClrMamePro format.
	"""

	pass

class ListXML:
	"""
	MAME List XML format.
	"""

	pass

class RomCenter:
	"""
	RomCenter format.
	"""

	pass

class GenericXML(DAT):
	"""
	Logiqx generic XML format.
	"""

	def __init__(self, datafile):
		"""
		Parse DAT file.
		"""

		self.name = Path(datafile).stem

		tree = ET.parse(datafile)
		root = tree.getroot()

		for header in root.findall('header'):
			self.header = {
				"name": header.find('name').text,
				"description": header.find('description').text,
				"version": header.find('version').text,
				"author": header.find('author').text,
				"homepage": header.find('homepage').text,
				"url": header.find('url').text
			}

		for game in root.iter('game'):
			count = sum(1 for _ in game.iter('rom'))
			directory = ""

			if count > 1:
				directory = "{0}/".format(game.get('name'))

			for rom in game.iter('rom'):
				self.sums.append({
					"crc": str(rom.get('crc').lower()),
					"md5": str(rom.get('md5').lower()),
					"name": str(directory + rom.get('name')),
					"size": int(rom.get('size')),
					"sha1": str(rom.get('sha1').lower())
				})
