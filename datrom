#!/usr/bin/env python3
# vim: set encoding=utf-8 noet ts=4
# TODO: NES headers

"""Identify ROM with DAT file."""

import binascii, os, sys
from dat import DAT

def identify(datafile, romfile):
	"""
	ROM file to identify.
	"""
	with open(romfile,'rb') as f:
		crc = "{:08x}".format(binascii.crc32(f.read()))
	print("CRC:  {0}".format(crc))

	size = os.path.getsize(romfile)
	print("Size: {0}".format(size))

	dat = DAT(datafile)
	name = dat.name(crc, size)
	print("Name: {0}".format(name))
	return name

def rename(romfile, name):
	"""
	Rename ROM file from name in DAT.
	"""
	romfile = os.path.abspath(romfile)
	name = os.path.join(os.path.dirname(romfile), name)
	os.rename(romfile, name)
	print("\nSource:      {0}\nDestination: {1}".format(romfile, name))

if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(add_help=False, description=__doc__)

	parser.add_argument('DAT', help=DAT.__doc__)
	parser.add_argument('ROM', help=identify.__doc__)

	parser.add_argument(
		'-h', '--help', action='help', default=argparse.SUPPRESS,
		help='Show this help message and exit.'
	)
	parser.add_argument(
		'-r', '--rename', action='store_true',
		help=rename.__doc__
	)

	args = parser.parse_args()
	name = identify(args.DAT, args.ROM)
	if args.rename:
		rename(args.ROM, name)
